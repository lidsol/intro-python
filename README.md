# Introducción a python

El repositorio tiene notas del curso, ejercicios y libros que fueron material
de apoyo para el curso. Los ejercicios tienen el formato `<numero>_<nombre>.py`
dónde `número` corresponde con el tema y `nombre` es un nombre descriptivo de que
ejercicio es.

## ¿Cómo ver las notas?

Las notas están disponibles en este repositorio en la carpeta llamada `notas/`.
Alternativamente se pueden encontrar en la siguiente liga de la página
[Google Colab](https://drive.google.com/drive/folders/1CZJ_yvp06y_95bffHLPxqBT3pCFZhWLp?usp=sharing).
Para poder ver las notas se requiere una cuenta de Google.

## ¿Cómo descargar el repositorio?

### Usando git

* Clonar el repositorio ejecutando en tu terminal `$git clone https://gitlab.com/lidsol/intro-python`

### Formato zip

* En la parte superior derecha del repositorio dar click al icono de flecha
hacia abajo y seleccionar la opción de `zip`. Descargará el repositorio en
un archivo comprimido
	
#### NOTA

* Para ver las notas desde tu computadora se requiere instalar `jupyter`. Intrucciones [aqui](https://jupyter.org/install).
* Si aún tienes dudas de como ver las notas puedes buscarnos en [LIDSOL](https://lidsol.org) o escribirme
a mi correo _diegobarriga [at] protonmail [dot] com_

## Temario visto

### Introducción

* ¿Qué es `python`?
    * Guido Van Rossum
    * Empresas que utilizan el lenguaje
* Entendiendo las computadoras (un poco)
    * Computadoras programables
    * Software y Hardware
        * CPU
        * Memoria principal
        * Memoria secundaria
        * Dispositivos de E/S
    * Lenguajes de programación
* Diseño de programas
    * Ciclo de desarrollo
    * ¿Cómo funciona un programa?
    * Programas en python como scripts
* Instalación de `python`
* Instalación de editor de texto
* Interprete de `python`

### Entrada, Procesamiento y Salida de datos

* Cadenas
* Impresión en pantalla
    * Uso de `print`
    * F-strings
    * Format
* Comentarios
    * De una línea
    * Multilínea
* Tipos de datos
    * Strings
    * Números enteros
    * Número de punto flotante
    * Booleanos
* Operadores Aritméticos
* Lectura desde el teclado

###  Estructuras de control

* If, If-else, If-elif-else
* While
* For

###  Secuencias

* Listas
* Tuplas
* Diccionarios
