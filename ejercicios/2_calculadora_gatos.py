# Calculadora de edad gatunos
# Por teclado se recibirá la edad del gato y se mostrará su edad humana
# Para menos de un año
# 2 meses - 3 edad
# 4 meses - 6 edad
# 6 meses - 9 edad
# 8 meses - 11 edad
# 10 meses - 13 edad
# 1 año - 15 edad
# 2 año - primer año + 9 edad
# 3 edad en adelante - segundo año + 4 edad

edad = float(input("Años del gato>> "))

if edad < 1:
    meses = edad * 12  # Convirtiendo los edad a meses
    if meses < 2:
        if meses == 0:
            print("0 edad graciosx :(")
        else:
            print("Es muy pequeño y adorable para saber O:")
    elif meses >= 2 and meses < 4:
        print("Tu gatito tiene ~3 años")
    elif meses >= 4 and meses < 6:
        print("Tu gatito tiene ~6 años")
    elif meses >= 6 and meses < 8:
        print("Tu gatito tiene ~9 años")
    elif meses >= 8 and meses < 10:
        print("Tu gatito tiene ~11 años")
    elif meses >= 10:
        print("Tu gatito tiene ~13 años")
elif edad == 1:
    print("Tu gatito tiene 15 años")
elif edad > 1:
    edad_base = 15
    if edad == 2:
        print(f"Tu gatito tiene {edad_base + 9} años")
    else:
        edad_base = 24
        edad_extra = edad - 2
        print(f"Tu gatito tiene {edad_base + (edad_extra * 4)} años")
