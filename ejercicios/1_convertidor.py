# Conversor de Grados C° a F°
# Ecuación ==> F° = C° * 1.8 + 32
print("Este programa convierte de gracos °C a °F")

cels_str = input("Ingresa los grados C°>> ")

cels_real = float(cels_str)

resultado = cels_real * 1.8 + 32

print("Los grados °C", cels_str, "son", resultado, "°F")
