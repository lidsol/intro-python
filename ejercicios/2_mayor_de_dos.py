# EJERCICIO: Encontrar el número mayor de dos números
# La usuaria dara por teclado 2 numeros enteros y el programa
# indicará cual es el mayor de los dos
# EXTRA: Mencionar si son iguales

num1 = int(input("Numero 1: "))
num2 = int(input("Numero 2: "))

if num1 > num2:
    print(num1)
elif num2 > num1:
    print(num2)
else:
    print("Son Iguales")
