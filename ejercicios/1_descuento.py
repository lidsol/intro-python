# Calculo de descuento
# El programa recibe dos números. El primero es el precio de un artículo
# y el segundo es un porcentaje de descuento en formato con punto decimal.
# Ej: 
# Da el precio real>> 100
# Da el descuento en porcentaje como número flotante>> 0.5
# El descuento es de $50.0
# El precio a pagar es de $50.0

valor = float(input("Da el precio real>> "))
porcentaje_descuento = float(input("Da el descuento en porcentaje como número flotante>> "))

descuento = valor * porcentaje_descuento

print("El descuento es de $", descuento)
print("El precio a pagar es $", valor - descuento)
