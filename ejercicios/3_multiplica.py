# EJERICIO: Multiplica
#
# Elaborar un programa que pida un número entero.
# Ese numero será multiplicado por 2 y desplegado en pantalla siempre y cuando
# sea menor a 1000
# Ej:
# 1
# 2
# 4
# 8
# ...
# 1000

numero = int(input("Ingresa un numero entero: "))
total = 0

print(numero)
while total < 1000:
    total = numero * 2
    if total >= 1000:
        break
    print(total)
    numero = total
