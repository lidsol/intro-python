# Ejercicio: Buscar los elementos duplicados en una lista y eliminarlos
numeros = [1, 1, 1, 2, 2, 3, 4, 4, 5, 6, 6, 6, 7, 7, 8]
no_duplicados = []

for numero in numeros:
    if numero not in no_duplicados:
        no_duplicados.append(numero)
    else:
        print("Numero", numero, "es duplicado")

print("Lista de números no duplicados:", no_duplicados)
