# Ejercicio: El juego de la adivinanza
# La usuaria deberá adivinar el número en el que estas pensando
# Ej:
# numero_pensado = 4
# Adivina en que número estoy pensando? 2
# Muy bajo
# Adivina en que número estoy pensando? 6
# Muy alto
# Adivina en que número estoy pensando? 4
# Adivinaste!!!
import random

print("Juego de la adivinanza")
print("(0) Fácil\t(1) Medio\t(2) Difícil")
dificultad = int(input("Selecciona la dificultad (0/1/2)>> "))
flag = 0
intentos = 0

if dificultad == 0:
    print("Adivina el numero que estoy pensado del 1 al 10")
    numero_pensado = random.randint(1, 10)  # Genera número aleatorio del 1 al 10
elif dificultad == 1:
    print("Adivina el numero que estoy pensado del 1 al 25")
    numero_pensado = random.randint(1, 25)  # Genera número aleatorio del 1 al 10
elif dificultad == 2:
    print("Adivina el numero que estoy pensado del 1 al 50")
    numero_pensado = random.randint(1, 50)  # Genera número aleatorio del 1 al 10

while flag != 1:
    numero_usuario = int(input("Tu numero: "))
    if numero_pensado == numero_usuario:
        print("Ganaste :)")
        flag = 1
    else:
        print("Estas mal :( INTENTOS =", intentos)
        intentos += 1

