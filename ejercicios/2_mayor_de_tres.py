# Este programa verifica el mayor de tres números

numero_1 = int(input("Ingresa el primer numero: "))
numero_2 = int(input("Ingresa el segundo numero: "))
numero_3 = int(input("Ingresa el tercer numero: "))

if numero_1 > numero_2:
    if numero_1 > numero_3:
        maximo = numero_1
    else:
        maximo = numero_3
elif numero_2 > numero_3:
    maximo = numero_2
else:
    maximo = numero_3

print(f"El mayor es {maximo}")
