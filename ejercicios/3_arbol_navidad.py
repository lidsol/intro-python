# Un arbol bonito

altura_arbol = int(input("Ingresa la altura del arbol> "))
espacios = altura_arbol - 1  # Espacios en blanco
relleno = 1
for i in range(altura_arbol):
    for j in range(espacios):  # Espacios por la izquierda
        print(" ", end='')
    for j in range(relleno):  # Relleno
        if i == 0:
            print("🌟", end='')
        else:
            if j % 2 == 0:  # Los impares
                print("°", end='')
            else:
                print("#", end='')
    for j in range(espacios):  # Espacios por la derecha
        print(" ", end='')
    relleno += 2  # El relleno aumenta en dos unidades
    espacios -= 1  # Los espacios disminuyen en una unidad
    print()  # Salto de línea
print(" " * (altura_arbol - 2), end='')
print("||")
