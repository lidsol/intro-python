# EJERCICIO
# Mostrar una tabla que vaya desde un inicio a un fin ingresados por teclado
# con sus valores elevados a otro numero que se ingrese por teclado a la derecha
# Ej:
# Numero   Cuadrado
# ------------------
# 1        1
# 2        4
# 3        9
# ...      ...
# 10       100

print("Tabla de potencias")

inicio = int(input("Valor de inicio de la tabla: "))
fin = int(input("Valor de fin de la tabla: "))
potenciador = int(input("Potencia: "))

print("Numero\tPotencia(" + str(potenciador) + ")")
print("=============================")
for numero in range(inicio, fin + 1):
    print(f"{numero}\t{numero ** potenciador}")  # Uso de F-strings
