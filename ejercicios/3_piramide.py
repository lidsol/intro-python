# Ejercicio: Piramide
# El programa recibe un número entero que será
# la base de la piramide
# Ej:
# Ingresa la base: 6
# *
# **
# ***
# ****
# *****
# ******

base = 6
for c in range(base):
    print("*" * (c + 1))
